package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"path"
	"strings"
)

func main() {
	ids := []string{
		"80023287",
		"81853217",
		"90304977",
		"90378924",
		// "92024273",
		"93089991",
		"93863669",
		"94657300",
		"94965685",
		"96471821",
		"102912361",
	}
	for i, id := range ids {
		url := fmt.Sprintf("https://gitlab.com/api/v4/projects/5767443/jobs/%s/artifacts/release", id)
		generateDoc(i+1, url, "", "")
	}
	newIds := []string{
		"112878601",
		"173735254",
		"189103652",
		"195746347",
		"208550213",
		"212456924",
		"272404997",
		"334001016",
	}
	for i, id := range newIds {
		linuxURL := fmt.Sprintf("https://gitlab.com/api/v4/projects/5767443/jobs/%s/artifacts/build/linux_amd64/release", id)
		darwinURL := fmt.Sprintf("https://gitlab.com/api/v4/projects/5767443/jobs/%s/artifacts/build/darwin_amd64/release", id)
		windowsURL := fmt.Sprintf("https://gitlab.com/api/v4/projects/5767443/jobs/%s/artifacts/build/windows_amd64/release.exe", id)
		generateDoc(i+1+len(ids), linuxURL, darwinURL, windowsURL)
	}

}

func generateDoc(n int, url string, darwinURL string, windowsURL string) {
	downloadFile("release", url)
	err := os.Chmod("release", 0755)
	if err != nil {
		panic(err)
	}
	versionBytes, err := exec.Command("./release", "short-version").Output()
	if err != nil {
		panic(err)
	}
	version := string(versionBytes)
	fmt.Println(version)
	docPath := path.Join(".", "site3", "content", "cli", version)
	downloadPath := path.Join(".", "site3", "static", "download", version)
	darwinPath := path.Join(".", "site3", "static", "download", version, "darwin")
	windowsPath := path.Join(".", "site3", "static", "download", version, "windows")
	paths := []string{docPath, downloadPath, darwinPath, windowsPath}
	for _, p := range paths {
		err = os.MkdirAll(p, 0755)
		if err != nil {
			panic(err)
		}
	}
	if len(darwinURL) > 0 {
		downloadFile(path.Join(darwinPath, "release"), darwinURL)
		downloadFile(path.Join(windowsPath, "release.exe"), windowsURL)
	}
	bin, err := ioutil.ReadFile("release")
	if err != nil {
		panic(err)
	}
	err = ioutil.WriteFile(path.Join(downloadPath, "release"), bin, 0755)
	if err != nil {
		panic(err)
	}
	onlyLinux := false
	if len(darwinURL) == 0 {
		onlyLinux = true
	}
	generateCommandDoc(version, onlyLinux, docPath, "help.md", 100*n, "./release", "help")

	pages := []string{
		"next-version",
		"changelog",
		"tag",
		"commit-and-tag",
		"tag-and-commit",
		"add-download",
		"add-download-link",
		"test-git",
		"test-api",
	}
	for i, p := range pages {
		filename := fmt.Sprintf("%s.md", p)
		generateCommandDoc(version, onlyLinux, docPath, filename, 100*n+i+2, "./release", "help", p)
	}
}

func generateCommandDoc(version string, onlyLinux bool, contentPath string, filename string, weight int, cmd string, args ...string) {
	helpBytes, err := exec.Command(cmd, args...).Output()
	if err != nil {
		panic(err)
	}
	content := fmt.Sprintf(`---
title: release %s
cliversions: ["%s"]
version: %s
shortTitle: %s
weight: %d
onlylinux: %t
layout: cli/single
---
%s`, strings.Join(args, " "), version, version, args[len(args)-1], weight, onlyLinux, string(helpBytes))
	err = ioutil.WriteFile(path.Join(contentPath, filename), []byte(content), 0644)
	if err != nil {
		panic(err)
	}
}

func downloadFile(filepath string, url string) error {
	os.Remove(filepath)

	// Create the file
	out, err := os.Create(filepath)
	if err != nil {
		return err
	}
	defer out.Close()

	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Write the body to file
	_, err = io.Copy(out, resp.Body)
	if err != nil {
		return err
	}

	return nil
}
