---
title: Stop initial development
description: What to do, when you're ready to release v1.0.0
weight: 32
---

# Initial development

Semantic versioning 2.0.0 states that

> Major version zero (0.y.z) is for initial development. Anything may change at any time. The public API should not be considered stable.

When major version is zero, go-semrel-gitlab does not automatically bump major version. Breaking changes cause only a minor bump.

## How to release v1.0.0

When you're ready to stop initial development and start using the normal rules for major version, set environment variable 
`GSG_INITIAL_DEVELOPMENT` to `false`. On next release, go-semrel-gitlab will bump directly to v1.0.0.

If major version is not 0, the value of `GSG_INITIAL_DEVELOPMENT` is ignored.