package actions

import (
	"encoding/base64"
	"fmt"
	"io/ioutil"

	"github.com/pkg/errors"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/juhani/go-semrel-gitlab/pkg/workflow"
	git "gopkg.in/src-d/go-git.v4"
)

var (
	// Mapping from git status code to gitlab commit action
	// only create and update are possible in this context
	fileStatusMap = map[git.StatusCode]gitlab.FileAction{
		git.Untracked: gitlab.FileCreate,
		git.Modified:  gitlab.FileUpdate,
	}
)

// Commit ..
type Commit struct {
	client  *gitlab.Client
	project string
	files   []string
	message string
	branch  string
	commit  *gitlab.Commit
}

// Do implements Action for Commit
func (action *Commit) Do() *workflow.ActionError {
	if action.commit != nil {
		return nil
	}
	commitActions, err := getCommitActionsForFiles(action.files)
	if err != nil {
		return workflow.NewActionError(errors.Wrap(err, "commit"), false)
	}
	if len(commitActions) == 0 {
		return workflow.NewActionError(errors.New("No changes to commit"), false)
	}
	options := &gitlab.CreateCommitOptions{
		Branch:        &action.branch,
		CommitMessage: &action.message,
		Actions:       commitActions,
	}
	commit, _, err := action.client.Commits.CreateCommit(action.project, options)
	if err != nil {
		// Not retrying creation of commit (at this stage at least).
		// After 502 the state of repository is not known, and it's difficult to check
		// without the sha of the commit.
		return workflow.NewActionError(errors.Wrap(err, "commit"), false)
	}
	action.commit = commit
	return nil
}

// Undo implements Action for Commit
func (action *Commit) Undo() error {
	if action.commit == nil {
		return nil
	}
	fmt.Printf(`
MANUAL ACTION REQUIRED!
Unable to clean up version bump commit of a failed release.
Commit SHA: %s\n`, action.commit.ID)
	return errors.New("commit cannot be undone")
}

// RefFunc returns accessor func to commit sha
func (action *Commit) RefFunc() func() string {
	return func() string {
		if action.commit == nil {
			return ""
		}
		return action.commit.ID
	}
}

// NewCommit ..
func NewCommit(client *gitlab.Client, project string, files []string, message string, branch string) *Commit {
	return &Commit{
		client:  client,
		project: project,
		files:   files,
		message: message,
		branch:  branch,
	}
}

func getCommitActionsForFiles(files []string) ([]*gitlab.CommitAction, error) {
	r, err := git.PlainOpen(".")
	if err != nil {
		return nil, err
	}
	wt, err := r.Worktree()
	if err != nil {
		return nil, err
	}
	wtStatus, err := wt.Status()
	if err != nil {
		return nil, err
	}
	actions := []*gitlab.CommitAction{}

	for _, file := range files {
		if filestatus, hasStatus := wtStatus[file]; hasStatus {
			if action, hasAction := fileStatusMap[filestatus.Worktree]; hasAction {
				bytes, err := ioutil.ReadFile(file)
				if err != nil {
					return nil, errors.Wrap(err, "readfile")
				}
				base64String := base64.StdEncoding.EncodeToString(bytes)
				actions = append(actions, &gitlab.CommitAction{
					Action:   action,
					FilePath: file,
					Content:  base64String,
					Encoding: "base64",
				})
			}
		}
	}
	return actions, nil
}
